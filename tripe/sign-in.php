<div class="page">
  <div class="sign-in-with">
    <div class="block-title"></div>
  </div>
  <div class="page-content">
    <!-- sign in -->
    <div class="sign-in">
      <div class="block-title block-title-page">
        <span class="overline-title">SIGN IN</span>
        <h3>Welcome Back</h3>
      </div>
      <div class="content-form">
        <div class="block">
          <form class="list">
            <ul>
              <li class="item-content item-input">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input type="email" id="emailin" class="emailin" placeholder="Email" required />
                  </div>
                </div>
              </li>
              <li class="item-content item-input">
                <div class="item-inner">
                  <div class="item-input-wrap">
                    <input type="password" id="password" class="passwodin" placeholder="Password"  required/>
                  </div>
                </div>
              </li>
            </ul>
            <div class="button-default">
              <button class="button" id="clickin" onclick="functionSignIn()">Sign In</button>
            </div>
          </form>
        </div>
      </div>

      <div class="link-to-sign-up">
        <div class="block">
          <div class="button-default">
            <a href="/sign-up/" class="button btn-secondary">Create Account</a>
          </div>
        </div>
      </div>
    </div>
    <!-- end sign in -->
  </div>
</div>

