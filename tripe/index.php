<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="default" />
    <meta
      http-equiv="Content-Security-Policy"
      content="default-src * 'self' 'unsafe-inline' 'unsafe-eval' data: gap:"
    />
    <link rel="icon" href="images/favicon.png" />
    <link
      href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap"
      rel="stylesheet"
    />
    <title>Tripe - Mobile Travel HTML Template</title>
    <link rel="stylesheet" href="css/framework7.bundle.min.css" />
    <link rel="stylesheet" href="css/ionicons.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  </head>
  
  <body>
    <div id="app">
<?php
session_start();
if (isset($_SESSION['user']) != null) {
    include 'sign-in.php';
} else {
     ?>
     <!-- Home  -->
      <div class="view view-main view-init ios-edges" data-url="/">
        <div class="page page-home page-with-subnavbar">
          <div class="toolbar tabbar tabbar-labels toolbar-bottom">
            <div class="toolbar-inner">
              <a href="#tab-favorite" class="tab-link">
                <i class="icon ion-ios-bookmark"></i>
                <span style="font-size: 12px;">Daftar Isi</span>
              </a>
              <a href="#tab-home" class="tab-link tab-link-active">
                <i class="icon ion-ios-home"></i>
                <span style="font-size: 12px;">Beranda</span>
              </a>
              <a href="#tab-nilai" class="tab-link">
                <i class="icon ion-ios-star"></i>
                <span style="font-size: 12px;">Nilai</span>
              </a>
            </div>
          </div>

          <div class="tabs">
            <div id="tab-home" class="tab tab-home tab-active">
              <!-- ========== TAB HOME ========== -->
              <div class="header-title">
                <div class="row">
                  <div class="col-70">
                    <div class="title-left">
                      <span class="title-name">Hi, Shinta Larasati</span>
                      <h2>Welcome</h2>
                    </div>
                  </div>
                  <div class="col-30">
                    <div class="profile-avatar">
                      <a href="#tab-profile" class="tab-link">
                        <div class="image">
                          <img src="images/icon-cover.png" alt="" />
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="header-searchbar">
                <form class="searchbar">
                  <div class="searchbar-inner">
                    <div class="searchbar-input-wrap">
                      <input type="search" placeholder="Pencarian kata" />
                      <i class="searchbar-icon"></i>
                      <span class="input-clear-button"></span>
                    </div>
                    <span class="searchbar-disable-button">Cancel</span>
                  </div>
                </form>
              </div>

              <div class="popular-destination">
                <div class="block-title">
                  <span class="overline-title">Text Narasi</span>
                  <h3>Cerita Fantasi</h3>
                </div>
                <div class="block">
                  <div
                    class="swiper-container swiper-init"
                    data-pagination='{"el": ".swiper-pagination"}'
                    data-space-between="20"
                    data-slides-per-view="auto"
                  >
                    <div class="swiper-pagination"></div>
                    <div class="swiper-wrapper">
                      <div class="swiper-slide">
                        <div class="full-mask"></div>
                        <img src="images/bg.png" alt="" />
                        <div class="caption">
                          <a href="/travel-details/">
                            <h4 class="title-tour-large">
                              Modul 1
                            </h4>
                          </a>
                          <div class="rate-destination">
                            <h5 style="color:white">Unsur - Unsur Cerita Fantasi</h5>
                          </div>
                          <div class="price-booking">
                            <h5>
                              10
                              <span>Halaman</span>
                            </h5>
                          </div>
                        </div>  
                      </div>
                      <div class="swiper-slide">
                        <div class="full-mask"></div>
                        <img src="images/bg.png" alt="" />
                        <div class="caption">
                          <a href="/travel-details/">
                            <h4 class="title-tour-large">
                              Latihan Modul 1
                            </h4>
                          </a>
                          <div class="rate-destination">
                            <ul>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><span>4.5</span></li>
                            </ul>
                          </div>
                          <div class="price-booking">
                            <h5>
                              20
                              <span>Soal</span>
                            </h5>
                          </div>
                          <div class="wrap-button">
                            <button class="button">Mulai</button>
                          </div>
                        </div>
                      </div>
                      <div class="swiper-slide">
                        <div class="full-mask"></div>
                        <img src="images/bg.png" alt="" />
                        <div class="caption">
                          <a href="/travel-details/">
                            <h4 class="title-tour-large">
                              Modul 2
                            </h4>
                          </a>
                          <div class="rate-destination">
                            <h5 style="color:white">Isi Cerita Fantasi</h5>
                          </div>
                          <div class="price-booking">
                            <h5>
                              20
                              <span>Halaman</span>
                            </h5>
                          </div>
                        </div>  
                      </div>
                      <div class="swiper-slide">
                        <div class="full-mask"></div>
                        <img src="images/bg.png" alt="" />
                        <div class="caption">
                          <a href="/travel-details/">
                            <h4 class="title-tour-large">
                              Latihan Modul 2
                            </h4>
                          </a>
                          <div class="rate-destination">
                            <ul>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><span>5</span></li>
                            </ul>
                          </div>
                          <div class="price-booking">
                            <h5>
                              25
                              <span>Soal</span>
                            </h5>
                          </div>
                          <div class="wrap-button">
                            <button class="button">Mulai</button>
                          </div>
                        </div>
                      </div>
                      <div class="swiper-slide">
                        <div class="full-mask"></div>
                        <img src="images/bg.png" alt="" />
                        <div class="caption">
                          <a href="/travel-details/">
                            <h4 class="title-tour-large">
                              Modul 3
                            </h4>
                          </a>
                          <div class="rate-destination">
                            <h5 style="color:white">Struktur dan Kebahasaan Cerita Fantasi</h5>
                          </div>
                          <div class="price-booking">
                            <h5>
                              20
                              <span>Halaman</span>
                            </h5>
                          </div>
                        </div>  
                      </div>
                      <div class="swiper-slide">
                        <div class="full-mask"></div>
                        <img src="images/bg.png" alt="" />
                        <div class="caption">
                          <a href="/travel-details/">
                            <h4 class="title-tour-large">
                              Latihan Modul 3
                            </h4>
                          </a>
                          <div class="rate-destination">
                            <ul>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><span>5</span></li>
                            </ul>
                          </div>
                          <div class="price-booking">
                            <h5>
                              30
                              <span>Soal</span>
                            </h5>
                          </div>
                          <div class="wrap-button">
                            <button class="button">Mulai</button>
                          </div>
                        </div>
                      </div>
                      <div class="swiper-slide">
                        <div class="full-mask"></div>
                        <img src="images/bg.png" alt="" />
                        <div class="caption">
                          <a href="/travel-details/">
                            <h4 class="title-tour-large">
                              Modul 4
                            </h4>
                          </a>
                          <div class="rate-destination">
                            <h5 style="color:white">Menulis Cerita Fantasi</h5>
                          </div>
                          <div class="price-booking">
                            <h5>
                              25
                              <span>Halaman</span>
                            </h5>
                          </div>
                        </div>  
                      </div>
                      <div class="swiper-slide">
                        <div class="full-mask"></div>
                        <img src="images/bg.png" alt="" />
                        <div class="caption">
                          <a href="/travel-details/">
                            <h4 class="title-tour-large">
                              Latihan Modul 4
                            </h4>
                          </a>
                          <div class="rate-destination">
                            <ul>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><i class="icon ion-ios-star"></i></li>
                              <li><span>5</span></li>
                            </ul>
                          </div>
                          <div class="price-booking">
                            <h5>
                              10
                              <span>Soal</span>
                            </h5>
                          </div>
                          <div class="wrap-button">
                            <button class="button">Mulai</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- ========== END TAB HOME ========== -->
            </div>

            <div id="tab-favorite" class="tab tab-favorite">
              <!-- ========== TAB FAVORITE ========== -->

              <div class="block-title">
                <span class="overline-title">DAFTAR ISI</span>
                <h3>Text Narasi (Cerita Fantasi)</h3>
              </div>

              <div class="favorite-category">
                <div
                  class="swiper-container swiper-container-offset swiper-init"
                  data-space-between="20"
                  data-slides-per-view="auto"
                >
                  <div class="swiper-wrapper">
                    <div class="swiper-slide" style="width: 100px;">
                      <div class="content content-active">
                        <span>Modul</span>
                      </div>
                    </div>
                    <div class="swiper-slide" style="width: 180px;">
                      <div class="content">
                        <span>Petunjuk Penggunaan</span>
                      </div>
                    </div>
                    <div class="swiper-slide" style="width: 160px;">
                      <div class="content">
                        <span>Kompetensi Inti</span>
                      </div>
                    </div>
                    <div class="swiper-slide" style="width: 165px;">
                      <div class="content">
                        <span>Kompetensi Dasar</span>
                      </div>
                    </div>
                    <div class="swiper-slide" style="width: 240px;">
                      <div class="content">
                        <span>Indikator Pencapaian Kompetensi</span>
                      </div>
                    </div>
                    <div class="swiper-slide" style="width: 200px;">
                      <div class="content">
                        <span>Tujuan Pembelajaran</span>
                      </div>
                    </div>
                    <div class="swiper-slide" style="width: 140px;">
                      <div class="content">
                        <span>Peta Konsep</span>
                      </div>
                    </div>
                    <div class="swiper-slide" style="width: 130px;">
                      <div class="content">
                        <span>Apresiasi</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="favorite-item">
                <div class="block">
                  <div class="content">
                    <div class="image">
                      <img src="images/icon-modul.jpeg" alt="" />
                      <div class="favorite-icon">
                        <i class="icon ion-ios-bookmark"></i>
                      </div>
                    </div>
                    <div class="text">
                      <a href="/travel-details/">
                        <h4>Modul 1</h4>
                      </a>
                      <p class="location">
                        <i class="icon ion-ios-book"></i>
                        Unsur - Unsur Cerita Fantasi
                      </p>
                      <div class="price-booking">
                        <h5>
                          10
                          <span>Halaman</span>
                        </h5>
                      </div>
                    </div>
                  </div>

                  <!-- divider space -->
                  <div class="divider-space-content"></div>
                  <!-- end divider space -->

                  <div class="content">
                    <div class="image">
                      <img src="images/icon-modul.jpeg" alt="" />
                      <div class="favorite-icon">
                        <i class="icon ion-ios-bookmark"></i>
                      </div>
                    </div>
                    <div class="text">
                      <a href="/travel-details/">
                        <h4>Modul 2</h4>
                      </a>
                      <p class="location">
                        <i class="icon ion-ios-book"></i>
                        Isi Cerita Fantasi
                      </p>
                      <div class="price-booking">
                        <h5>
                          20
                          <span>Halaman</span>
                        </h5>
                      </div>
                    </div>
                  </div>

                  <!-- divider space -->
                  <div class="divider-space-content"></div>
                  <!-- end divider space -->
                  <div class="content">
                    <div class="image">
                      <img src="images/icon-modul.jpeg" alt="" />
                      <div class="favorite-icon">
                        <i class="icon ion-ios-bookmark"></i>
                      </div>
                    </div>
                    <div class="text">
                      <a href="/travel-details/">
                        <h4>Modul 3</h4>
                      </a>
                      <p class="location">
                        <i class="icon ion-ios-book"></i>
                        Struktur dan Kebahasaan Cerita Fantasi
                      </p>
                      <div class="price-booking">
                        <h5>
                          15
                          <span>Halaman</span>
                        </h5>
                      </div>
                    </div>
                  </div>

                  <!-- divider space -->
                  <div class="divider-space-content"></div>
                  <!-- end divider space -->

                  <div class="content">
                    <div class="image">
                      <img src="images/icon-modul.jpeg" alt="" />
                      <div class="favorite-icon">
                        <i class="icon ion-ios-bookmark"></i>
                      </div>
                    </div>
                    <div class="text">
                      <a href="/travel-details/">
                        <h4>Modul 4</h4>
                      </a>
                      <p class="location">
                        <i class="icon ion-ios-book"></i>
                        Menulis Cerita Fantasi
                      </p>
                      <div class="price-booking">
                        <h5>
                          17
                          <span>Halaman</span>
                        </h5>
                      </div>
                    </div>
                  </div>

                  <!-- divider space -->
                  <div class="divider-space-content"></div>
                  <!-- end divider space -->
                </div>
              </div>
              <!-- ========== END TAB FAVORITE ========== -->
            </div>

            <div id="tab-explore" class="tab tab-explore">
              <!-- ========== TAB EXPLORE ========== -->

              <div class="explore-header-slide">
                <div class="block-title">
                  <span class="overline-title">EXPLORE</span>
                  <h3>Explore the world</h3>
                </div>

                <div
                  class="swiper-container swiper-container-offset swiper-init"
                  data-space-between="20"
                  data-slides-per-view="auto"
                >
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                      <div class="half-mask half-mask-v"></div>
                      <img src="images/mountain.jpg" alt="" />
                      <div class="caption">
                        <h2>Explore Mountain</h2>
                        <p>Explore mountains together around the world</p>
                      </div>
                    </div>
                    <div class="swiper-slide">
                      <div class="half-mask half-mask-v"></div>
                      <img src="images/forest.jpg" alt="" />
                      <div class="caption">
                        <h2>Explore Forest</h2>
                        <p>
                          Explore the Forest through various recreational
                          activities
                        </p>
                      </div>
                    </div>
                    <div class="swiper-slide">
                      <div class="half-mask half-mask-v"></div>
                      <img src="images/beach.jpg" alt="" />
                      <div class="caption">
                        <h2>Explore Beach</h2>
                        <p>The best beaches & islands to explore</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="explore-too">
                <div class="block-title">
                  <h3>Explore Too</h3>
                </div>
                <div
                  class="swiper-container swiper-container-offset swiper-init"
                  data-space-between="20"
                  data-slides-per-view="auto"
                >
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                      <a href="/explore-category-place/">
                        <div class="half-mask"></div>
                        <img src="images/explore-too1.jpg" alt="" />
                        <div class="caption">
                          <h4>Hotels</h4>
                          <span>98 Hotels</span>
                        </div>
                      </a>
                    </div>
                    <div class="swiper-slide">
                      <a href="/explore-category-place/">
                        <div class="half-mask"></div>
                        <img src="images/explore-too2.jpg" alt="" />
                        <div class="caption">
                          <h4>Restaurants</h4>
                          <span>210 Restaurants</span>
                        </div>
                      </a>
                    </div>
                    <div class="swiper-slide">
                      <a href="/explore-category-place/">
                        <div class="half-mask"></div>
                        <img src="images/explore-too3.jpg" alt="" />
                        <div class="caption">
                          <h4>Tours</h4>
                          <span>1040 Tours</span>
                        </div>
                      </a>
                    </div>
                    <div class="swiper-slide">
                      <a href="/explore-category-place/">
                        <div class="half-mask"></div>
                        <img src="images/explore-too4.jpg" alt="" />
                        <div class="caption">
                          <h4>Lodging</h4>
                          <span>140 Lodging</span>
                        </div>
                      </a>
                    </div>
                    <div class="swiper-slide">
                      <a href="/explore-category-place/">
                        <div class="half-mask"></div>
                        <img src="images/explore-too5.jpg" alt="" />
                        <div class="caption">
                          <h4>Activities</h4>
                          <span>1400 Activities</span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="country-category">
                <div class="block-title">
                  <h3>Popular Destinations</h3>
                </div>
                <div
                  class="swiper-container swiper-container-offset swiper-init"
                  data-space-between="20"
                  data-slides-per-view="auto"
                >
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                      <a href="/country-details/">
                        <div class="full-mask"></div>
                        <img src="images/africa.jpg" alt="" />
                        <div class="caption">
                          <h4>Africa</h4>
                          <span>98 Tours</span>
                        </div>
                      </a>
                    </div>
                    <div class="swiper-slide">
                      <a href="/country-details/">
                        <div class="full-mask"></div>
                        <img src="images/australia.jpg" alt="" />
                        <div class="caption">
                          <h4>Australia</h4>
                          <span>98 Tours</span>
                        </div>
                      </a>
                    </div>
                    <div class="swiper-slide">
                      <a href="/country-details/">
                        <div class="full-mask"></div>
                        <img src="images/maldives.jpg" alt="" />
                        <div class="caption">
                          <h4>Maldives</h4>
                          <span>98 Tours</span>
                        </div>
                      </a>
                    </div>
                    <div class="swiper-slide">
                      <a href="/country-details/">
                        <div class="full-mask"></div>
                        <img src="images/japan.jpg" alt="" />
                        <div class="caption">
                          <h4>Japan</h4>
                          <span>98 Tours</span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="banner-offer">
                <div class="block">
                  <div class="content">
                    <img src="images/banner.png" alt="" />
                  </div>
                </div>
              </div>

              <div class="recommended-hotel">
                <div class="block-title">
                  <h3>Recommended Hotel</h3>
                </div>
                <div
                  class="swiper-container swiper-container-offset swiper-init"
                  data-space-between="20"
                  data-slides-per-view="auto"
                >
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                      <img src="images/hotel-h1.jpg" alt="" />
                      <div class="text">
                        <p class="location">
                          <i class="icon ion-ios-pin"></i>
                          South Africa
                        </p>
                        <h4>National Hotel of Africa</h4>
                        <div class="price-booking">
                          <h5>
                            $95
                            <span>/Night</span>
                          </h5>
                        </div>
                      </div>
                    </div>
                    <div class="swiper-slide">
                      <img src="images/hotel-h2.jpg" alt="" />
                      <div class="text">
                        <p class="location">
                          <i class="icon ion-ios-pin"></i>
                          Australia
                        </p>
                        <h4>Little Airport Hotel</h4>
                        <div class="price-booking">
                          <h5>
                            $105
                            <span>/Night</span>
                          </h5>
                        </div>
                      </div>
                    </div>
                    <div class="swiper-slide">
                      <img src="images/hotel-h3.jpg" alt="" />
                      <div class="text">
                        <p class="location">
                          <i class="icon ion-ios-pin"></i>
                          Maldivies
                        </p>
                        <h4>Panorama Hotel</h4>
                        <div class="price-booking">
                          <h5>
                            $97
                            <span>/Night</span>
                          </h5>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- ========== END TAB EXPLORE ========== -->
            </div>

            <div id="tab-profile" class="tab tab-profile">
              <!-- ========== TAB PROFILE ========== -->
              <div class="header-image">
                <img src="images/icon-cover.png" alt="" />
              </div>
              <div class="profile-avatar-name">
                <div class="block">
                  <img src="images/icon-cover.png" alt="" />
                  <ul>
                    <li>
                      <div class="title-name">
                        <h4>Shinta Larasati</h4>
                        <div class="location">
                          <p>
                            <i class="icon ion-ios-pin"></i>
                            Indonesia
                          </p>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="icon-edit">
                        <a href="/profile-settings/">
                          <i class="icon ion-ios-images"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="profile-statistics">
                <div class="block">
                  <div class="row">
                    <div class="col-33">
                      <div class="content">
                        <h5>35</h5>
                        <p>Trips</p>
                      </div>
                    </div>
                    <div class="col-33">
                      <div class="content">
                        <h5>90</h5>
                        <p>Favorites</p>
                      </div>
                    </div>
                    <div class="col-33">
                      <div class="content">
                        <h5>211</h5>
                        <p>Photos</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="menu-list">
                <div class="block">
                  <div class="list">
                    <ul>
                      <li>
                        <a href="/password/" class="item-link item-content">
                          <div class="item-media">
                            <i class="icon ion-ios-settings"></i>
                          </div>
                          <div class="item-inner">
                            <div class="item-title">Manage Password</div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="item-link item-content">
                          <div class="item-media">
                            <i class="icon ion-ios-log-out"></i>
                          </div>
                          <div class="item-inner">
                            <div class="item-title">Logout</div>
                          </div>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- ========== END TAB PROFILE ========== -->
            </div>
            <div id="tab-nilai" class="tab tab-nilai">
              <!-- ========== TAB PROFILE ========== -->
              <div class="header-image" style="height:50px;">
                <img src="images/ff5b44.png" alt="" />
              </div>
              <div class="profile-avatar-name">
                <div class="block">
                  <img src="images/icon-cover.png" alt="" />
                  <ul>
                    <li>
                      <div class="title-name">
                        <h4>Shinta Larasati</h4>
                        <div class="location">
                          <p>
                            <i class="icon ion-ios-pin"></i>
                            Indonesia
                          </p>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="icon-edit">
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="profile-statistics">
                <div class="block">
                  <div class="row" style="margin-top:-15px; border-radius:10px; padding:10px; box-sizing: border-box; box-shadow: 3px 3px 5px 3px rgb(223 220 220);">
                    <div class="col">
                      <div class="content">
                        <h5>211</h5>
                        <p>Latihan Modul 1</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="profile-statistics">
                <div class="block">
                  <div class="row" style="margin-top:-20px; border-radius:10px; padding:10px; box-sizing: border-box; box-shadow: 3px 3px 5px 3px rgb(223 220 220);">
                    <div class="col">
                      <div class="content">
                        <h5>35</h5>
                        <p>Latihan Modul 2</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="profile-statistics">
                <div class="block">
                  <div class="row" style="margin-top:-20px; border-radius:10px; padding:10px; box-sizing: border-box; box-shadow: 3px 3px 5px 3px rgb(223 220 220);">
                    <div class="col">
                      <div class="content">
                        <h5>211</h5>
                        <p>Latihan Modul 3</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php
}
?>
    </div>
    <!-- script -->
    <script>
      function functionSignIn () {
        var email = $('#emailin').val()
        var password = $('#password').val()
        window.location.href = "http://localhost/modul-online/tripe/proses.php?email="+email+"&password="+password
      }
    </script>
    <script src="js/framework7.bundle.min.js"></script>
    <script src="js/routes.js"></script>
    <script src="js/app.js"></script>
    <!-- end script -->
  </body>
</html>
